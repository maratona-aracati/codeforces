#include <bits/stdc++.h>
using namespace std;

int mdc(int a, int b){
  return (b == 0 ? a : mdc(b, a%b)); //b == 0 ? Caso sim, retorne a, caso n�o, retorne mdc(b, a%b)
}

int main() {

    int a, b;

    cin>>a>>b;

    int cnt = 0;

    while(a <= b) {
        cnt++;
        a*=3;
        b*=2;
    }

    cout<<cnt<<endl;

}
