#include <bits/stdc++.h>
using namespace std;

int main() {

    int n, m, k;

    cin>>n>>m>>k;

    int comp = min(m, k);

    if(n > comp)
        cout<<"No"<<endl;
    else
        cout<<"Yes"<<endl;

    return 0;
}
