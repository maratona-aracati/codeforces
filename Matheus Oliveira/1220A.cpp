#include <bits/stdc++.h>
using namespace std;

int main() {

    int n;

    cin>>n;

    map<char, int> mp;

    //for(int i = 0; i < n; ++i) {
        string s;
        cin>>s;
        for(char x : s) {
            mp[x]++;
        }
    //}

    int total = mp['n'] + mp['z'];

    for(int i = 0; i < total; ++i) {
        if(i < mp['n']) cout<<1;
        else cout<<0;
        if(i < total - 1) cout<<" ";
    }
    cout<<endl;

    return 0;

}
