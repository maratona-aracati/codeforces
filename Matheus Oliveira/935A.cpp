#include <bits/stdc++.h>
using namespace std;

int main() {

    int n;

    cin>>n;

    set<int> divs;

    divs.insert(1);

    for(int i = 2; i * i <= n; ++i) {
        if(n%i==0) divs.insert(i);
        if(n%(n/i)==0) divs.insert(n/i);
    }

    cout<<divs.size()<<endl;
}
