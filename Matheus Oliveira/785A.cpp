#include <bits/stdc++.h>
using namespace std;

int main() {

    int n;
    cin>>n;

    map<string, int> mp;

    mp["Cube"] = 6;
    mp["Tetrahedron"] = 4;
    mp["Octahedron"] = 8;
    mp["Dodecahedron"] = 12;
    mp["Icosahedron"] = 20;

    int ans = 0;

    for(int i = 0; i < n; ++i) {
        string a;
        cin>>a;
        ans += mp[a];
    }
    cout<<ans<<endl;
}
