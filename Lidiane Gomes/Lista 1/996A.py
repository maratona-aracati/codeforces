n = int(input())

cont = 0
while(n):
    if(n < 5):
        cont += n
        n = 0
    elif(n < 10):
        cont += n // 5
        n %= 5
    elif(n < 20):
        cont += n // 10
        n %= 10
    elif(n < 100):
        cont += n // 20
        n %= 20
    else:
        cont += n // 100
        n %= 100
    
print(cont)
