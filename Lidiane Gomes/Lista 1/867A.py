n = int(input())
str = input()
sf = 0
fs = 0

for i in range(n - 1):
    if(str[i:i+2] == 'FS'):
        fs += 1
    if(str[i:i+2] == 'SF'):
        sf += 1

print(sf > fs and 'YES' or 'NO')
