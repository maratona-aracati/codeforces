
x = input().split()
maior = 0
pos_maior = -1

for i in range(0, len(x), 1):
    x[i] = int(x[i])
    if(x[i] > maior):
        maior = x[i]
        pos_maior = i

del(x[pos_maior])

a = int(x[1]) - maior + int(x[0])
b = int(x[2]) - maior + int(x[0])
c = maior - int(x[0])
res = [a,b,c]
print(a,b,c)

# a + b = x -> a  =  x - b -> a = x - (z - c) -> a = x - z + c
# a + c = y - > a = y - c -> a = y - (K - x) -> a = y - K + x
# b + c = z -> b = z - c -> b = z - K + x
# a + b + c = K
# x - z + c + b + c = K
# x - z + 2c + b = K
# x  + c = K
# c = K - x
# a = y - K + x
# b = z - K + x
