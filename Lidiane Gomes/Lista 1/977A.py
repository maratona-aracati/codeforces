nums = input().split(' ')
n = int(nums[0])
k = int(nums[1])

for i in range(k):
    if(n % 10 != 0):
        n -= 1
    else:
        n //= 10

print(n)
