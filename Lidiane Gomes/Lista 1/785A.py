polyhedrons = {
    'Tetrahedron': 4,
    'Cube': 6,
    'Octahedron': 8,
    'Dodecahedron':12,
    'Icosahedron' :20
}

def total_faces(n):
    resp = 0
    for i in range(n):
        str = input()
        resp += polyhedrons[str]

    return resp


n = int(input())
print(total_faces(n))
