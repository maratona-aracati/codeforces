#include <bits/stdc++.h>
#define pb push_back
using namespace std;

int main() {   

  vector<int> vet;
  int a1, b1, c1;
  for(int i = 0; i < 4; ++i) {
    cin >> a1;
    vet.pb(a1);
  }
  sort(vet.begin(), vet.end());
  
  a1 = vet[3] - vet[2];
  b1 = vet[3] - vet[1];
  c1 = vet[3] - vet[0];

  cout << a1 << " " << b1 << " " << c1 << endl;
  
  return 0;
}