#include <bits/stdc++.h>
using namespace std;

int main() {

  int n;
  cin >> n;

  
  for(int i = 0; i < n; ++i) {
    // adiciona o espaço no começo de cada frase, desde que não seja a primeira frase
    if(i > 0) cout << " ";
    
    if(i%2 == 0) 
      cout << "I hate ";
    else 
      cout << "I love ";

    // se for a ultima frase, printa "it", se não "that"
    if(i+1 >= n) cout << "it";
    else cout << "that";
  }

  cout << endl;

  return 0;
}
