#include <bits/stdc++.h>
#define pb push_back
using namespace std;

int main() {

  int n, res, aux = 0;
  cin >> n;

  aux += n/100;
  n = n%100;

  aux += n/20;
  n = n%20;

  aux += n/10;
  n = n%10;

  aux += n/5;
  n = n%5;

  aux += n;
  
  cout << aux << endl;

  return 0;
}