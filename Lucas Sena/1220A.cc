#include <bits/stdc++.h>
#define pb push_back
using namespace std;

int main() {   

  int x, n = 0, z = 0;
  string in;
  cin >> x;
  cin >> in;

  for(int i = 0; i < in.length(); i++) {
    if(in[i] == 'z') z++;
    else if(in[i] == 'n') n++;
  }

  while(n--) {
    cout << 1;
    if(n-1 >= 0 || z > 0) cout << " ";
  }

  while(z--) {
    cout << 0;
    if(z-1 >= 0) cout << " ";
  }
  cout << endl;

  return 0;
}