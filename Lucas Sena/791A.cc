#include <bits/stdc++.h>
using namespace std;

int main() {

  int a, b, cont = 0;
  cin >> a >> b;

  while(true) {
    a *= 3;
    b *= 2;
    cont++;
    if(a > b) break;
  }

  cout << cont << endl;

  return 0;
}