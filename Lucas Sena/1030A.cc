#include <bits/stdc++.h>
using namespace std;

int main() {   

  int n, aux;
  string out = "";
  cin >> n;

  while(n--) {
    cin >> aux;
    
    if(aux == 0 && !out.size()) 
      out = "EASY";
    else if(aux == 1) 
      out = "HARD";
  }
  cout << out << endl;  

  return 0;
}